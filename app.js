// Реализовать функцию для создания объекта "пользователь".
// Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser. При вызове функция должна спросить
// у вызывающего имя и фамилию. Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и
// lastName. Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя, соединенную
// с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko). Создать пользователя с помощью
// функции createNewUser(). Вызвать у пользователя функцию getLogin(). Вывести в консоль результат выполнения функции.,




class CreateNewUser {
    constructor (firstName, secondName, birthday) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.birthday = birthday;
    }
    getLogin() {
        return this.firstName[0].toLowerCase() + this.secondName.toLowerCase();
    }
    getAge() {
        return new Date().getFullYear() - this.birthday.split(".")[2];
    }
    getPassword() {
        return this.firstName[0].toLowerCase() + this.secondName.toLowerCase() + this.birthday.slice(6);
    }
}


const newUser = new CreateNewUser (prompt("Напишите ваше имя"), prompt("Напишите вашу фамилию"), prompt("Напишите дату вашего рождения в формате dd.mm.yyyy.")) 
console.log(newUser.getLogin(), newUser.getAge(), newUser.getPassword());



// Функция фильра массива


function filterBy(arr, type) {
    if(Array.isArray(arr)) {
        const newArr = arr.filter(el => (typeof el) === type);
        console.log(newArr);
    } else {
        alert(`error`)
    }
}

const arr = ['hello', 'world', 23, 43, null, undefined, function(){}, String]
const filter = prompt("Выберите тип данный", "undefined, null, number, string, object, function, boolean")

filterBy(arr, filter)
